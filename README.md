# p4 - Python Path Planning Project - DEPRECATED


This project has moved to GitHub at https://github.com/ssardina-planning/p4-simulator

The repo for Goal Recognition using p4 can be found here: https://github.com/ssardina-planning/p4-simulator-gr




----------------------------

## Screenshots

Blue agent navigating to destination:

![screenshot](https://bitbucket.org/ssardina-research/p4-simulator/raw/master/docs/screenshots/screenshot01.png)

After arrival, area searched (closed list) is shown in yellow:


![screenshot](https://bitbucket.org/ssardina-research/p4-simulator/raw/master/docs/screenshots/screenshot02.png)


A random agent:

![screenshot](https://bitbucket.org/ssardina-research/p4-simulator/raw/master/docs/screenshots/screenshot03.png)


Use via terminal to do batch testing (many scenario problems in one map):

![screenshot](https://bitbucket.org/ssardina-research/p4-simulator/raw/master/docs/screenshots/screenshot03.png)

